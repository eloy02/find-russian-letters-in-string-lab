﻿#include <string>
#include <iostream>
#include <algorithm>
#include <windows.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	setlocale(LC_ALL, "Russian");

	std::string letters = "йцукенгшщзхъфывапролджэячсмитьбю";
	std::string result_letters;

	std::string input_string;

	std::cout << "Введите строку: ";
	std::getline(std::cin, input_string);

	for (auto s : input_string)
	{
		if (s == '.') //по условию точка - конец строки. все, что после точки отбрасываем
			break;

		auto pos = letters.find(s);

		//если string.find() не находит элемент, то возвращает std::string::npos
		//если находит, то возвращает позицию в строке
		if (pos != std::string::npos)
		{
			auto let = letters[pos];

			//если буква уже есть в массиве, то не добавляем
			if (result_letters.find(let) == std::string::npos)
				result_letters.push_back(let);
		}
	}

	std::sort(result_letters.begin(), result_letters.end());

	std::cout << "Найденные буквы: " << result_letters;
}